#!/bin/bash

##############
# Install deps
##############
# Ubuntu
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install python3-pip jq -y
#####################

# Amazon Linux (RHEL) - NAT instances
# yum update -y
# epel provides python-pip & jq
# yum install -y epel-release
# yum install python-pip jq -y
#####################

pip install --upgrade awscli

##############

cat <<"EOF" > /home/${ssh_user}/update_ssh_authorized_keys.sh
#!/bin/bash

set -e

BUCKET_NAME=${s3_bucket_name}
BUCKET_URI=${s3_bucket_uri}
SSH_USER=${ssh_user}
MARKER="# KEYS_BELOW_WILL_BE_UPDATED_BY_TERRAFORM"
KEYS_FILE=/home/$SSH_USER/.ssh/authorized_keys
TEMP_KEYS_FILE=$(mktemp /tmp/authorized_keys.XXXXXX)
PUB_KEYS_DIR=/home/$SSH_USER/pub_key_files/
PATH=/usr/local/bin:$PATH

[[ -z $BUCKET_URI ]] && BUCKET_URI="s3://$BUCKET_NAME/"

mkdir -p $PUB_KEYS_DIR

# Add marker, if not present, and copy static content.
grep -Fxq "$MARKER" $KEYS_FILE || echo -e "\n$MARKER" >> $KEYS_FILE
line=$(grep -n "$MARKER" $KEYS_FILE | cut -d ":" -f 1)
head -n $line $KEYS_FILE > $TEMP_KEYS_FILE

# Synchronize the keys from the bucket.
aws s3 sync --delete $BUCKET_URI $PUB_KEYS_DIR
for filename in $PUB_KEYS_DIR/*; do
    [ -f "$filename" ] || continue
    sed 's/\n\?$/\n/' < $filename >> $TEMP_KEYS_FILE
done

# Move the new authorized keys in place.
chown $SSH_USER:$SSH_USER $KEYS_FILE
chmod 600 $KEYS_FILE
mv $TEMP_KEYS_FILE $KEYS_FILE
if [[ $(command -v "selinuxenabled") ]]; then
    restorecon -R -v $KEYS_FILE
fi
EOF

cat <<"EOF" > /home/${ssh_user}/.ssh/config
Host *
    StrictHostKeyChecking no
EOF
chmod 600 /home/${ssh_user}/.ssh/config
chown ${ssh_user}:${ssh_user} /home/${ssh_user}/.ssh/config

chown ${ssh_user}:${ssh_user} /home/${ssh_user}/update_ssh_authorized_keys.sh
chmod 755 /home/${ssh_user}/update_ssh_authorized_keys.sh

# Execute now
su ${ssh_user} -c /home/${ssh_user}/update_ssh_authorized_keys.sh

# Be backwards compatible with old cron update enabler
if [ "${enable_hourly_cron_updates}" = 'true' -a -z "${keys_update_frequency}" ]; then
  keys_update_frequency="0 * * * *"
else
  keys_update_frequency="${keys_update_frequency}"
fi

# Add to cron
if [ -n "$keys_update_frequency" ]; then
  croncmd="/home/${ssh_user}/update_ssh_authorized_keys.sh"
  cronjob="$keys_update_frequency $croncmd"
  ( crontab -u ${ssh_user} -l | grep -v "$croncmd" ; echo "$cronjob" ) | crontab -u ${ssh_user} -
fi

wget https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb -O /home/ubuntu/amazon-cloudwatch-agent.deb

dpkg -i -E /home/ubuntu/amazon-cloudwatch-agent.deb

cat <<"EOF" > /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
{
    "logs": {
        "logs_collected": {
            "files": {
                "collect_list": [
                    {
                        "file_path": "/var/log/auth.log",
                        "log_group_name": "bastion-log-group",
                        "log_stream_name": "{instance_id}-auth-log",
                        "retention_in_days": -1,
                        "timestamp_format": "%b %d %H:%M:%S"
                    }
                ]
            }
        }
    },
    "metrics": {
        "namespace": "System/Linux",
        "append_dimensions": {
            "InstanceId": "$${aws:InstanceId}"
        },
        "metrics_collected": {
            "disk": {
                "resources": [
                    "/"
                ],
                "measurement": [
                    {"name": "free", "rename": "DiskSpaceAvailable"},
                    {"name": "used", "rename": "DiskSpaceUsed"},
                    {"name": "used_percent", "rename": "DiskSpaceUtilization"}
                ]
            },
            "mem": {
                "measurement": [
                    {"name": "available", "rename": "MemoryAvailable"},
                    {"name": "used", "rename": "MemoryUsed"},
                    {"name": "used_percent", "rename": "MemoryUtilization"}
                ]
            }
        }
    }
}
EOF

chmod go+r /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json

/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json

# Append addition user-data script
${additional_user_data_script}
